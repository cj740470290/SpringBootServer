package com.example.demo.service;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.example.demo.mapper.BookMapper;

@Service
public class BookService {
  // mybatis的Servic写法
  // @Autowired
  // private BookMapper mapper;
  
  // public List<Book> getAll(){
  //   List<Book> list = new ArrayList<Book>();
  //   list = mapper.selectAll();
  //   return list;
  // }
}
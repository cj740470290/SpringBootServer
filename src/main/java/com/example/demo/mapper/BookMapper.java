package com.example.demo.mapper;

import java.util.List;
import java.util.Map;

import com.example.demo.bean.Book;
import com.example.demo.bean.User;
import com.example.demo.bean.Comment;
import com.example.demo.bean.Record;
// import org.apache.ibatis.annotations.Select;
// import org.apache.ibatis.annotations.Mapper;

// @Mapper
public interface BookMapper {
    // @Select("SELECT * FROM test")
    List<Book> search(Map map);
    List<User> getUser(Map map);
    List<Record> getRecord(Map map);
    List<Comment> getComment(Map map);
    User selectUser(Map map);
    Integer getBookTotal(Map map);
    Integer getUserTotal(Map map);
    Integer getRecordTotal(Map map);
    Integer getCommentTotal(Map map);
    Integer getStock(Map map);
    List<Integer> getRecordId(Map map);
    void create(Book book);
    void addUser(Map map);
    void addComment(Map map);
    void addRecord(Map map);
    void deleteBook(Book book);
    void deleteComment(Map map);
    void updateUser(Map map);
    void update(Book book);
    void updateRecord(Map map);
}
# libraryManage

#### 介绍
基于Vue3，ant-design-vue2开发的图书管理/图书借阅系统
地址https://gitee.com/cj740470290/libraryManage<br>
演示地址(账号密码都是admin)http://cj740470290.gitee.io/librarymanage/<br>
项目截图：
![Image text](https://gitee.com/cj740470290/libraryManage/raw/master/demo.png)

#### 软件架构
使用最新的Vue3.0开发

#### 环境
1.  java8(下载地址https://mirrors.tuna.tsinghua.edu.cn/Adoptium/8/jdk/x64/windows/)
2.  mysql(下载地址http://mirrors.sohu.com/mysql/MySQL-5.5/)
3.  navicat(mysql可视化工具https://gitee.com/cj740470290/disk/raw/master/Navicat%20Premium.zip)
4.  nodejs后端服务(项目地址https://gitee.com/cj740470290/libraryServer)
5.  java后端服务(项目地址https://gitee.com/cj740470290/SpringBootServer)

#### 安装教程

1.  npm换国内的源，提高装包速度(npm config set registry https://registry.npm.taobao.org)
2.  clone项目到本地，在项目根路径打开命令行，输入npm install
3.  双击根目录下的run.bat运行项目
4.  用navicat连接数据库，本项目账号为root，密码为root，新建数据库library，点击"表"目录，右击选择"运行 SQL 文件"(加参数--default-character-set=utf8防止中文乱码)，运行后端项目的library.sql文件，即可生成表
5.  maven换阿里云镜像，提高装包速度
D:\IntelliJIDEA2018.1.8\plugins\maven\lib\maven3\conf\settings.xml
```xml
<mirror>
   <id>alimaven</id>
   <name>aliyun maven</name>
   <url>http://maven.aliyun.com/nexus/content/groups/public/</url>
   <mirrorOf>central</mirrorOf>
</mirror>
```
6.  clone后端项目到本地，使用idea启动，或者在项目根路径打开命令行，输入mvn clean package
